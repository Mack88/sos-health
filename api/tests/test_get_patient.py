from fastapi.testclient import TestClient
from main import app
from queries.patients import PatientRepository


client = TestClient(app)


class EmptyPatient:
    def get_all(self):
        return []


class CreatePatient:
    def create(self, patient):
        result = {
            'id': 1,
            "uid": "7222078e-8d95-44b6-8a65-9439c5fbd889",
            'account_uid': "wasd",
            'f_name': "Someone",
            'l_name': "Nobody",
            'email': "SomebodyNobody@gmail.com",
            'address': "999 Persons blv",
            'phone': "368-3678-3333",
            "dob": "2024-03-18",
            'p_initial': "SN",
            }
        result.update(patient)
        return result


def test_get_all_patients():
    app.dependency_overrides[PatientRepository] = EmptyPatient
    response = client.get("/patient")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_create_patients():
    app.dependency_overrides[PatientRepository] = CreatePatient

    json = {
        'account_uid': "wasd",
        'f_name': "Someone",
        'l_name': "Nobody",
        'email': "SomebodyNobody@gmail.com",
        'address': "999 Persons blv",
        'phone': "368-3678-3333",
        "dob": "2024-03-18",
        'p_initial': "SN",
    }

    expected = {
        'id': 1,
        "uid": "7222078e-8d95-44b6-8a65-9439c5fbd889",
        'account_uid': "wasd",
        'f_name': "Someone",
        'l_name': "Nobody",
        'email': "SomebodyNobody@gmail.com",
        'address': "999 Persons blv",
        'phone': "368-3678-3333",
        "dob": "2024-03-18",
        'p_initial': "SN",
    }

    response = client.post("/patient", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
