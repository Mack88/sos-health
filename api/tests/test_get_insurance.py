from fastapi.testclient import TestClient
from main import app
from queries.insurance import InsuranceRepository


client = TestClient(app)


class EmptyInsurance:
    def get_all(self):
        return []


class CreateInsurance:
    def create(self, insurance):
        result = {
            "id": 1,
            "uid": "0fdafa60-ef08-4a57-a411-be98312a95dc",
            "name": "bub",
            "clinic_uid": "slug"
            }
        result.update(insurance)
        return result


def test_get_all_insurance():
    app.dependency_overrides[InsuranceRepository] = EmptyInsurance
    response = client.get("/insurance")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_create_insurance():
    app.dependency_overrides[InsuranceRepository] = CreateInsurance

    json = {
        "name": "bub",
        "clinic_uid": "slug"
    }

    expected = {
        "id": 1,
        "uid": "0fdafa60-ef08-4a57-a411-be98312a95dc",
        "name": "bub",
        "clinic_uid": "slug"
    }

    response = client.post("/insurance", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
