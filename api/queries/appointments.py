from pydantic import BaseModel
from typing import Optional, List, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class AppointmentIn(BaseModel):
    patient_uid: str
    provider_uid: str
    clinic_uid: str
    day: date
    status: str
    type: str


class FullAppointmentOut(BaseModel):
    uid: str
    day: date
    status: str
    type: str
    p_initial: str
    name: str
    clinic_uid: str
    patient_uid: str
    provider_uid: str


class AppointmentOut(BaseModel):
    id: int
    uid: str
    patient_uid: str
    provider_uid: str
    clinic_uid: str
    day: date
    status: str
    type: str


class AppointmentRepository:

    def get_one(self, uid: str) -> Optional[AppointmentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            SELECT *
                            FROM appointment
                            WHERE uid = %s;
                        """,
                        [uid]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_appointment(record)
        except Exception as e:
            print("error", e, "has occurred :(")
            return {"message": "sorry, couldn't find that appointment :("}

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM appointment
                        WHERE id = %s;
                        """,
                        [id]
                    )
                    return True
        except Exception as e:
            print("error", e, "has occurred :(")
            return False

    def update(
        self, uid: str, appointment: AppointmentIn
    ) -> Union[Error, AppointmentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            UPDATE appointment
                            SET patient_uid = %s,
                            provider_uid = %s,
                            clinic_uid  = %s,
                            day = %s,
                            status = %s,
                            type = %s
                            WHERE uid =%s
                            RETURNING id;
                        """,
                        [
                         appointment.patient_uid,
                         appointment.provider_uid,
                         appointment.clinic_uid,
                         appointment.day,
                         appointment.status,
                         appointment.type,
                         uid
                        ]
                    )
                    id = result.fetchone()[0]
                    old_data = appointment.dict()
                    return AppointmentOut(uid=uid, id=id, **old_data)
        except Exception as e:
            print("error", e, "has occurred :(")

    def get_all(self) -> Union[Error, List[AppointmentOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            SELECT *
                            FROM appointment;
                        """
                    )
                    result = []
                    for record in db:
                        appointment = AppointmentOut(
                            id=record[0],
                            uid=record[1],
                            patient_uid=record[2],
                            provider_uid=record[3],
                            clinic_uid=record[4],
                            day=record[5],
                            status=record[6],
                            type=record[7]
                        )
                        result.append(appointment)
                    return result
        except Exception as e:
            print("error", e, "has occurred :(")

    def create(self, appointment: AppointmentIn) -> AppointmentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO appointment
                        (patient_uid, provider_uid,
                        clinic_uid, day, status, type)
                        VALUES
                        (%s, %s, %s, %s, %s, %s)
                        RETURNING *;
                    """,
                    [
                        appointment.patient_uid,
                        appointment.provider_uid,
                        appointment.clinic_uid,
                        appointment.day,
                        appointment.status,
                        appointment.type
                    ]
                )
                appointmentt = result.fetchone()
                return self.record_to_appointment(appointmentt)

    def appointment_in_to_out(self, id: int, uid: str, sample: AppointmentIn):
        old_data = sample.dict()
        return AppointmentOut(id=id, uid=uid, **old_data)

    def record_to_appointment(self, record):
        return AppointmentOut(
            id=record[0],
            uid=record[1],
            patient_uid=record[2],
            provider_uid=record[3],
            clinic_uid=record[4],
            day=record[5],
            status=record[6],
            type=record[7]
        )

    def get_by_clinic(self, c_uid) -> Union[Error, List[FullAppointmentOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            SELECT a.uid, a.day, a.status,
                                a.type, p.p_initial, v.name,
                                a.clinic_uid, a.patient_uid, a.provider_uid
                            FROM appointment as a
                            JOIN patient as p
                            ON a.patient_uid = p.uid
                            JOIN provider as v
                            ON a.provider_uid = v.uid
                            JOIN clinic as c
                            ON a.clinic_uid = c.uid
                            WHERE c.uid = %s
                            ORDER BY a.day;
                        """,
                        [c_uid]
                    )
                    result = []
                    for record in db:
                        appointment = FullAppointmentOut(
                            uid=record[0],
                            day=record[1],
                            status=record[2],
                            type=record[3],
                            p_initial=record[4],
                            name=record[5],
                            clinic_uid=record[6],
                            patient_uid=record[7],
                            provider_uid=record[8]
                        )
                        result.append(appointment)
                    return result
        except Exception as e:
            print("error", e, "has occurred :(")

    def get_by_patient(self, p_uid) -> Union[Error, List[AppointmentOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            SELECT *
                            FROM appointment as a
                            LEFT JOIN patient as p
                            ON a.clinic_uid = p.uid
                            WHERE p.uid = %s
                        """,
                        [p_uid]
                    )
                    result = []
                    for record in db:
                        appointment = AppointmentOut(
                            id=record[0],
                            uid=record[1],
                            patient_uid=record[2],
                            provider_uid=record[3],
                            clinic_uid=record[4],
                            day=record[5],
                            status=record[6],
                            type=record[7]
                        )
                        result.append(appointment)
                    if result == []:
                        return {"message": "no appointments for this patient"}
                    return result
        except Exception as e:
            print("error", e, "has occurred :(")
