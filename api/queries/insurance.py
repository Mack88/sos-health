from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class InsuranceIn(BaseModel):
    name: str
    clinic_uid: str


class InsuranceOut(BaseModel):
    id: int
    uid: str
    name: str
    clinic_uid: str


class PutInsuranceOut(BaseModel):
    name: str


class PutInsuranceIn(BaseModel):
    name: str


class InsuranceRepository:

    def get_all(self) -> List[InsuranceOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, uid, name, clinic_uid
                    FROM insurance
                    ORDER BY id;
                    """
                )
                insurances = []
                for record in db:
                    insurance = InsuranceOut(
                        id=record[0],
                        uid=record[1],
                        name=record[2],
                        clinic_uid=record[3],
                    )
                    insurances.append(insurance)
                return insurances

    def create(self, insurance: InsuranceIn) -> InsuranceOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO insurance
                    (name, clinic_uid)
                    VALUES
                    (%s, %s)
                    RETURNING *;
                    """,
                    [insurance.name, insurance.clinic_uid],
                )
                record = result.fetchone()
                return InsuranceOut(
                    id=record[0],
                    uid=record[1],
                    name=record[2],
                    clinic_uid=record[3]
                )

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM insurance
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception as L:
            print("L", L)
            return False

    def get_one(self, id: int) -> Optional[InsuranceOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            uid,
                            name,
                            clinic_uid
                        FROM insurance
                        WHERE id = %s
                        """,
                        [id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return InsuranceOut(
                        id=record[0],
                        uid=record[1],
                        name=record[2],
                        clinic_uid=record[3],
                    )
        except Exception as L:
            print("L", L)
            return {"message": "Take the L"}

    def update(
        self, id: int, insurance: PutInsuranceIn
    ) -> Union[Error, PutInsuranceOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE insurance
                        SET name = %s
                        WHERE id = %s
                        """,
                        [insurance.name, id],
                    )
                    old_data = insurance.dict()
                    return PutInsuranceOut(id=id, **old_data)
        except Exception as L:
            print("TAKE L", L)
            return {"message": "L Take It"}
