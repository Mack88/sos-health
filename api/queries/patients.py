from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool
from datetime import date


class Error(BaseModel):
    message: str


class PatientIn(BaseModel):
    account_uid: str
    f_name: str
    l_name: str
    email: str
    address: str
    phone: str
    dob: date
    p_initial: str


class PatientOut(BaseModel):
    id: int
    uid: str
    account_uid: str
    f_name: str
    l_name: str
    email: str
    address: str
    phone: str
    dob: date
    p_initial: str


class PutPatientIn(BaseModel):
    f_name: str
    l_name: str
    email: str
    address: str
    phone: str
    dob: date
    p_initial: str


class PutPatientOut(BaseModel):
    f_name: str
    l_name: str
    email: str
    address: str
    phone: str
    dob: date
    p_initial: str


class PatientRepository:

    def create(self, patient: PatientIn) -> Union[Error, PatientOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO patient
                        (
                        account_uid,
                        f_name,
                        l_name,
                        email,
                        address,
                        phone,
                        dob,
                        p_initial
                        )
                        VALUES
                        (%s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [
                            patient.account_uid,
                            patient.f_name,
                            patient.l_name,
                            patient.email,
                            patient.address,
                            patient.phone,
                            patient.dob,
                            patient.p_initial
                        ],
                    )
                    record = result.fetchone()
                    return PatientOut(
                        id=record[0],
                        uid=record[1],
                        account_uid=record[2],
                        f_name=record[3],
                        l_name=record[4],
                        email=record[5],
                        address=record[6],
                        phone=record[7],
                        dob=record[8],
                        p_initial=record[9]
                    )
        except Exception as L:
            print("TAKE L", L)
            return {"message": "TAKE L, TAKE It"}

    def get_all(self) -> List[PatientOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id,
                            uid,
                            account_uid,
                            f_name,
                            l_name,
                            email,
                            address,
                            phone,
                            dob,
                            p_initial
                        FROM patient
                        ORDER BY id;
                        """
                    )
                    patients = []
                    for record in db:
                        patient = PatientOut(
                            id=record[0],
                            uid=record[1],
                            account_uid=record[2],
                            f_name=record[3],
                            l_name=record[4],
                            email=record[5],
                            address=record[6],
                            phone=record[7],
                            dob=record[8],
                            p_initial=record[9],
                        )
                        patients.append(patient)
                    return patients
        except Exception as L:
            print("TAKE L", L)
            return {"message": "TAKE L, TAKE It"}

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM patient
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception as L:
            print("L", L)
            return False

    def get_one(self, uid: str) -> Optional[PatientOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM patient
                        WHERE uid = %s
                        """,
                        [uid],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return PatientOut(
                        id=record[0],
                        uid=record[1],
                        account_uid=record[2],
                        f_name=record[3],
                        l_name=record[4],
                        email=record[5],
                        address=record[6],
                        phone=record[7],
                        dob=record[8],
                        p_initial=record[9],
                    )
        except Exception as L:
            print("L", L)
            return {"message": "Take the L"}

    def update(self, id: int,
               patient: PutPatientIn) -> Union[Error, PutPatientOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE patient
                        SET f_name = %s,
                                l_name = %s,
                                email = %s,
                                address = %s,
                                phone = %s,
                                dob = %s,
                                p_initial = %s
                        WHERE id = %s
                        """,
                        [patient.f_name,
                            patient.l_name,
                            patient.email,
                            patient.address,
                            patient.phone,
                            patient.dob,
                            patient.p_initial,
                            id],
                    )
                    old_data = patient.dict()
                    return PutPatientOut(id=id, **old_data)
        except Exception as L:
            print("TAKE L", L)
            return {"message": "TAKE L, TAKE It"}
