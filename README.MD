# SOS HealthCare

# Members

-   Grayson York
-   Cian Windous
-   Timothy McCormack
-   Philip Rha

Grayson - Appointments backend and Appointments front end.

Cian - Patients backend and Navbar/accounts/update password front end.

Timothy - Provider backend and Clinic Details front end.

Philip - Clinic backend and Navbar/login/signup front end.

## Design

-   [API design](docs/apis.md)
-   [GHI](docs/ghi.md)

## Intended market

SOS HealthCare is an application centralized on streamling the process of scheduling appointments for patients by facilitating a seamless access to healthcare services. Its design aims to expedite the delivery of healthcare.

## Functionality

-   Clinics will be directed to the main page upon accessing the application. Here, they can either log in with existing credentials or create a new account. Once logged in, they will be directed to the appointment page.
    -   On the appointment page, clinics have the ability to view all scheduled appointments.
    -   They can create new appointments
    -   Register patients
    -   Clinics can easily toggle between viewing upcoming appointments and accessing records of previous appointments.
-   Within the application, clinics have the option to navigate to Clinic Details.
    -   Clinics can view information on all providers and insurances associated with their clinic.
    -   Clinics have the capability to create new providers and insurances directly within the Clinic Details section.
-   Clinics can also navigate to Account Details
    -   Clinics can view their current account information including name, email, and address.
    -   Clinics can update their account password for enhanced security measures.

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. https://software-oppssum-squad.gitlab.io/sos-health/
