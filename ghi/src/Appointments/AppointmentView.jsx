import { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'

const API_HOST = import.meta.env.VITE_API_HOST

function Appointments() {
    const [appointments, setAppointments] = useState([])
    const [clinic, setClinic] = useState([])
    const [status, setStatus] = useState('')
    const [apptToggle, setApptToggle] = useState(true)
    const [searchField, setSearchField] = useState('Select a Category')

    const getAppointments = async function () {
        const clinicUrl = `${API_HOST}/clinictoken`
        const clinicResponse = await fetch(clinicUrl, {
            credentials: 'include',
        })

        let clinic_data = []
        if (clinicResponse.ok) {
            const data = await clinicResponse.json()
            const dat = data.clinic
            for (let k in dat) {
                clinic_data.push(dat[k])
                setClinic(clinic_data)
            }
        }
        const url = `${API_HOST}/appointment/clinic/${clinic_data[1]}`
        const response = await fetch(url, {
            credentials: 'include',
        })
        const appointments = await response.json()
        console.log(appointments)
        let createdAppt = []
        console.log('base appointments:', createdAppt)
        for (let a of appointments) {
            console.log(Date.now(), Date.parse(a.day))
            if (apptToggle == true) {
                if (Date.now() > Date.parse(a.day)) {
                    createdAppt.push(a)
                }
            } else if (Date.now() <= Date.parse(a.day)) {
                createdAppt.push(a)
            }
        }
        console.log('setting appointments to:', createdAppt)
        setAppointments(createdAppt)
    }

    useEffect(() => {
        getAppointments()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleStatusChange = (event) => {
        const value = event.target.value
        setStatus(value)
    }

    const handleStatusSubmit = async function (appointment) {
        console.log('attempting to change status')
        event.preventDefault()
        const data = {}
        data.patient_uid = appointment.patient_uid
        data.provider_uid = appointment.provider_uid
        data.clinic_uid = appointment.clinic_uid
        data.day = appointment.day
        data.status = status
        data.type = appointment.type
        const url = `${API_HOST}/appointment/${appointment.uid}`
        console.log('request:', url, data)
        const fetchOptions = {
            method: 'put',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchOptions)
        if (response.ok) {
            const newStatus = await response.json()
            console.log('status changed to ', newStatus)

            setStatus('')
            getAppointments()
        }
    }

    const handleApptToggleChange = () => {
        setApptToggle(!apptToggle)
        getAppointments()
    }

    const handleSetSearchPatient = () => {
        setSearchField('Patient Initials')
    }

    const handleSetSearchProvider = () => {
        setSearchField('Provider Name')
    }

    const handleSetSearchType = () => {
        setSearchField('Appointment Type')
    }

    const handleSetSearchDay = () => {
        setSearchField('Appointment Day')
    }

    const handleSetSearchStatus = () => {
        setSearchField('Patient Status')
    }
    // need to css in a warning when there is an active appt in previous, and completed in upcoming

    return (
        <section className="bg-gray-50 min-h-96 dark:bg-gray-900 p-3 sm:p-5">
            <div className="mx-auto max-w-screen-xl px-4 lg:px-12">
                <div className="text-3xl font-extrabold leading-tight tracking-tight text-gray-900 sm:text-4xl dark:text-white">
                    welcome, {clinic[2]}{' '}
                </div>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                    <div className="w-full md:w-1/2">
                        <form className="max-w-lg mx-auto">
                            <div className="flex">
                                <label
                                    htmlFor="dropdown-search"
                                    className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
                                >
                                    Search bar
                                </label>
                                <button
                                    id="button-dropdown"
                                    data-dropdown-toggle="dropdown"
                                    className="flex-shrink-0 z-10 inline-flex items-center py-2.5 px-4 text-sm font-medium text-center text-gray-900 bg-gray-100 border border-gray-300 rounded-s-lg hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700 dark:text-white dark:border-gray-600"
                                    type="button"
                                >
                                    {searchField}
                                    <svg
                                        className="w-2.5 h-2.5 ms-2.5"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 10 6"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="m1 1 4 4 4-4"
                                        />
                                    </svg>
                                </button>
                                <div
                                    id="dropdown"
                                    className="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700"
                                >
                                    <ul
                                        className="py-2 text-sm text-gray-700 dark:text-gray-200"
                                        aria-labelledby="button-dropdown"
                                    >
                                        <li>
                                            <button
                                                type="button"
                                                onClick={handleSetSearchPatient}
                                                className="inline-flex w-full px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                            >
                                                Patient Initials
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                type="button"
                                                onClick={
                                                    handleSetSearchProvider
                                                }
                                                className="inline-flex w-full px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                            >
                                                Provider Name
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                type="button"
                                                onClick={handleSetSearchType}
                                                className="inline-flex w-full px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                            >
                                                Appointment Type
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                type="button"
                                                onClick={handleSetSearchDay}
                                                className="inline-flex w-full px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                            >
                                                Appointment Day
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                type="button"
                                                onClick={handleSetSearchStatus}
                                                className="inline-flex w-full px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                            >
                                                Appointment Status
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div className="relative w-full">
                                    <input
                                        type="search"
                                        id="dropdown-search"
                                        className="block p-2.5 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-e-lg border-s-gray-50 border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-s-gray-700  dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:border-blue-500"
                                        placeholder="Find appointments"
                                        required
                                    />
                                    <button
                                        type="submit"
                                        className="absolute top-0 end-0 p-2.5 text-sm font-medium h-full text-white bg-blue-700 rounded-e-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                    >
                                        <svg
                                            className="w-4 h-4"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 20 20"
                                        >
                                            <path
                                                stroke="currentColor"
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                strokeWidth="2"
                                                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                                            />
                                        </svg>
                                        <span className="sr-only">Search</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="w-full md:w-auto flex flex-col md:flex-row space-y-2 md:space-y-0 items-stretch md:items-center justify-end md:space-x-3 flex-shrink-0">
                        <NavLink
                            className="text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                            aria-current="page"
                            to="/appointments/create"
                        >
                            Schedule an Appointment
                        </NavLink>
                    </div>
                    <div className="w-full md:w-auto flex flex-col md:flex-row space-y-2 md:space-y-0 items-stretch md:items-center justify-end md:space-x-3 flex-shrink-0">
                        <NavLink
                            className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                            aria-current="page"
                            to="/patients/create"
                        >
                            Register a Patient
                        </NavLink>
                    </div>
                </div>
                <div className="bg-white dark:bg-gray-800 relative shadow-md sm:rounded-lg overflow-hidden">
                    <div className="overflow-x-auto">
                        <header className="Appointment-bg">
                            <h1 className="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                Appointments
                            </h1>
                            <div>
                                <label className="inline-flex items-center me-5 cursor-pointer">
                                    <input
                                        type="checkbox"
                                        value=""
                                        className="sr-only peer"
                                    />
                                    <div
                                        onClick={handleApptToggleChange}
                                        className="relative w-11 h-6 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-focus:ring-4 peer-focus:ring-teal-300 dark:peer-focus:ring-teal-800 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-teal-600"
                                    ></div>
                                    <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300">
                                        Upcoming/Previous
                                    </span>
                                </label>
                            </div>
                        </header>
                        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" className="px-4 py-3">
                                        patient
                                    </th>
                                    <th scope="col" className="px-4 py-3">
                                        provider
                                    </th>
                                    <th scope="col" className="px-4 py-3">
                                        type
                                    </th>
                                    <th scope="col" className="px-4 py-3">
                                        day
                                    </th>
                                    <th scope="col" className="px-4 py-3">
                                        status
                                    </th>
                                    <th scope="col" className="px-4 py-3"></th>
                                    <th scope="col" className="px-4 py-3"></th>
                                </tr>
                            </thead>
                            {appointments.map((appointment) => {
                                return (
                                    <tbody key={appointment.uid}>
                                        <tr className="border-b dark:border-gray-700">
                                            <td className="px-4 py-3">
                                                {appointment.p_initial}
                                            </td>
                                            <td className="px-4 py-3">
                                                {appointment.name}
                                            </td>
                                            <td className="px-4 py-3">
                                                {appointment.type}
                                            </td>
                                            <td className="px-4 py-3">
                                                {appointment.day}
                                            </td>
                                            <td className="px-4 py-3">
                                                {appointment.status}
                                            </td>
                                            <td className="inline-flex items-center p-0.5 text-sm font-medium text-center text-gray-500 hover:text-gray-800 rounded-lg focus:outline-none dark:text-gray-600 dark:hover:text-gray-600">
                                                <select
                                                    onChange={
                                                        handleStatusChange
                                                    }
                                                >
                                                    <option>
                                                        select new status
                                                    </option>
                                                    <option value="active">
                                                        active
                                                    </option>
                                                    <option value="canceled">
                                                        canceled
                                                    </option>
                                                    <option value="completed">
                                                        completed
                                                    </option>
                                                </select>
                                            </td>
                                            <td>
                                                <button
                                                    className="focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:focus:ring-yellow-900"
                                                    onClick={() =>
                                                        handleStatusSubmit(
                                                            appointment
                                                        )
                                                    }
                                                >
                                                    change status
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                )
                            })}
                        </table>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Appointments
