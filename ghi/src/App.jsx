// This makes VSCode check types as if you are using TypeScript
//@ts-check
// eslint-disable-next-line no-unused-vars
import React from 'react'
import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import Login from './ClinicFrontEnd/Login'
import Signup from './ClinicFrontEnd/Signup'

// All your environment variables in vite are in this object

// When using environment variables, you should do a check to see if
// they are defined or not and throw an appropriate error message

/**
 * This is an example of using JSDOC to define types for your component
 * @typedef {{module: number, week: number, day: number, min: number, hour: number}} LaunchInfo
 * @typedef {{launch_details: LaunchInfo, message?: string}} LaunchData
 *
 * @returns {React.ReactNode}
 */
function App() {
    return (
        <BrowserRouter>
            <div className="dark bg-neutral-600 bg-cover h-screen">
                <Routes>
                    <Route path="/login" element={<Login />} />
                    <Route path="/signup" element={<Signup />} />
                    <Route path="/*" element={<MainPage />} />
                </Routes>
            </div>
        </BrowserRouter>
    )
}

export default App
