import { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'

const API_HOST = import.meta.env.VITE_API_HOST

function ClinicDetail() {
    const [providers, setProviders] = useState([])
    const [insurances, setInsurances] = useState([])
    const [clinic, setClinic] = useState(null)

    const getClinicDetails = async () => {
        try {
            const clinicResponse = await fetch(`${API_HOST}/clinictoken`, {
                credentials: 'include',
            })

            if (clinicResponse.ok) {
                const clinicData = await clinicResponse.json()
                const clinic = clinicData.clinic
                setClinic(clinic)
                setProviders(clinic.providers)
                setInsurances(clinic.insurances)
            } else {
                console.error(
                    'Failed to fetch clinic details:',
                    clinicResponse.statusText
                )
            }
        } catch (error) {
            console.error('Error fetching clinic details:', error)
        }
    }

    useEffect(() => {
        getClinicDetails()
    }, [])

    useEffect(() => {
        const getData = async (url, setter) => {
            try {
                const response = await fetch(url)

                if (response.ok) {
                    const data = await response.json()
                    setter(data)
                } else {
                    console.error('Failed to fetch data:', response.statusText)
                }
            } catch (error) {
                console.error('Error fetching data:', error)
            }
        }

        getData(`${API_HOST}/provider`, setProviders)
        getData(`${API_HOST}/insurance`, setInsurances)
    }, [])

    if (!clinic) {
        return <div>Loading...</div>
    }

    return (
        <section className="bg-gray-50 min-h-96 dark:bg-gray-900 p-3 sm:p-5">
            <div className="mx-auto max-w-screen-xl px-4 lg:px-12">
                <div className="text-3xl font-extrabold leading-tight tracking-tight text-gray-900 sm:text-4xl dark:text-white">
                    welcome, {clinic.name}
                </div>
                <div className="container mx-auto flex flex-wrap justify-between relative shadow-md sm:rounded-lg overflow-hidden">
                    <div className="w-full lg:w-1/2 px-4 mb-8">
                        <div className="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                            <h2 className="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                Providers
                            </h2>
                            <div className="w-full md:w-auto flex flex-col md:flex-row space-y-2 md:space-y-0 items-stretch md:items-center justify-end md:space-x-3 flex-shrink-0">
                                <NavLink
                                    to="/provider/create"
                                    className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                                >
                                    Add Provider
                                </NavLink>
                            </div>
                        </div>
                        <div className="overflow-x-auto">
                            <div className="bg-white dark:bg-gray-800 relative shadow-md sm:rounded-lg overflow-hidden">
                                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 ">
                                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                        <tr>
                                            <th
                                                scope="col"
                                                className="px-4 py-3"
                                            >
                                                ID
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-4 py-3"
                                            >
                                                Name
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-4 py-3"
                                            >
                                                Email
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-4 py-3"
                                            >
                                                Specialty
                                            </th>
                                            <th
                                                scope="col"
                                                className="px-4 py-3"
                                            >
                                                Phone
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {providers &&
                                            providers.map((provider) => (
                                                <tr
                                                    className="border-b dark:border-gray-700"
                                                    key={provider.id}
                                                >
                                                    <td className="px-4 py-3">
                                                        {provider.id}
                                                    </td>
                                                    <td className="px-4 py-3">
                                                        {provider.name}
                                                    </td>
                                                    <td className="px-4 py-3">
                                                        {provider.email}
                                                    </td>
                                                    <td className="px-4 py-3">
                                                        {provider.specialty}
                                                    </td>
                                                    <td className="px-4 py-3">
                                                        {provider.phone}
                                                    </td>
                                                </tr>
                                            ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="w-full lg:w-1/2 px-4 mb-8">
                        <div className="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                            <h2 className="px-4 py-3 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                Insurances
                            </h2>
                            <div className="w-full md:w-auto flex flex-col md:flex-row space-y-2 md:space-y-0 items-stretch md:items-center justify-end md:space-x-3 flex-shrink-0">
                                <NavLink
                                    to="/insurance/create"
                                    className="text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                                >
                                    Add Insurance
                                </NavLink>
                            </div>
                        </div>
                        <div className="overflow-x-auto">
                            <div className="bg-white dark:bg-gray-800 relative shadow-md sm:rounded-lg overflow-hidden">
                                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                        <tr>
                                            <th className="px-4 py-3">ID</th>
                                            <th className="px-4 py-3">Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {insurances &&
                                            insurances.map((insurance) => (
                                                <tr
                                                    key={insurance.id}
                                                    className="border-b dark:border-gray-700"
                                                >
                                                    <td className="px-4 py-3">
                                                        {insurance.id}
                                                    </td>
                                                    <td className="px-4 py-3">
                                                        {insurance.name}
                                                    </td>
                                                </tr>
                                            ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ClinicDetail
