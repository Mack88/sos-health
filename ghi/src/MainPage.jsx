import Navbar from './Navbar'
import { useEffect } from 'react'
// eslint-disable-next-line no-unused-vars
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import ClinicDetail from './ClinicDetails/ClinicDetailView'
import CreateProvider from './ClinicDetails/ProviderPost'
import CreateInsurance from './ClinicDetails/InsurancePost'
import CreateAppointment from './Appointments/AppointmentPost'
import CreatePatient from './Appointments/PatientPost'
import Appointments from './Appointments/AppointmentView'
import Accounts from './ClinicFrontEnd/Accounts'


const API_HOST = import .meta.env.VITE_API_HOST





function MainPage() {

    // const [token, setToken] = useState([])
    const nav = useNavigate()

    const checkToken = async function () {
        console.log("checking token")
        const tokenResponse = await fetch(
            `${API_HOST}/clinictoken`,
            {credentials: 'include'}
        )
        const data = await tokenResponse.json()
        console.log(data == null)
        if (data == null) {
            console.log("attempting redriect to login")
            nav('/login')
        }
    }
    useEffect(() => {
        checkToken()
    })



    return (
        <div>
            <Navbar />
            <div className="container mx-auto py-10 my-10">
                <Routes>
                    <Route path="/clinicdetail" element={<ClinicDetail />} />
                    <Route
                        path="/provider/create"
                        element={<CreateProvider />}
                    />
                    <Route
                        path="/insurance/create"
                        element={<CreateInsurance />}
                    />
                    <Route path="/appointments" element={<Appointments />} />
                    <Route
                        path="/appointments/create"
                        element={<CreateAppointment />}
                    />
                    <Route
                        path="/patients/create"
                        element={<CreatePatient />}
                    />
                    <Route path="/appointments" element={<Appointments />} />
                    <Route
                        path="/appointments/create"
                        element={<CreateAppointment />}
                    />
                    <Route
                        path="/patients/create"
                        element={<CreatePatient />}
                    />
                    <Route path="/account" element={<Accounts />} />
                </Routes>
            </div>
        </div>
    )
}

export default MainPage
