# APIs Design

# Appointments

-   **Method**: `GET`, `GET`, `POST` , `PUT`, `DELETE`
-   **Path**: /api/queries/appointments

    Input:

    ```json
    {
    "patient_uid": str,
    "provider_uid": str,
    "clinic_uid": str,
    "day": date,
    "status": str,
    "type": str
    }
    ```

    Output:

    ```json
    {
    "uid": str,
    "day": date,
    "status": str,
    "type": str,
    "p_initial": str,
    "name": str,
    "clinic_uid": str,
    "patient_uid": str,
    "provider_uid": str
    }
    ```

*   The Appointments API allows users to retrieve individual appointments, retrieve all appointments at once, create new appointments, update existing ones, and remove appointments.

# Clinic

-   **Method**: `GET`, `GET`, `POST` , `PUT`, `DELETE`
-   **Path**: /api/queries/clinic

    Input:

    ```json
    {
    "name": str,
    "username": str,
    "password": str,
    "email": str,
    "address": str,
    "photo": str
    }
    ```

    Output:

    ```json
    {
    "id": str,
    "uid": str,
    "name": str,
    "username": str,
    "email": str,
    "address": str,
    "photo": str
    }
    ```

*   The Clinic API enables users to retrieve clinic information, to create new clinics users will need to enter in all the necessary information, update existing clinic, and delete clinic.

# Insurance

-   **Method**: `GET`, `GET`, `POST` , `PUT`, `DELETE`
-   **Path**: /api/queries/insurance

    Input:

    ```json
    {
    "name": str,
    "clinic_uid": str
    }
    ```

    Output:

    ```json
    {
    "id": int,
    "uid": str,
    "name": str,
    "clinic_uid": str
    }
    ```

*   The Insurance API is able to retrive individual insurance or all insurance records at once, able to create, update, and delete insurance.

# Patients

-   **Method**: `GET`, `GET`, `POST` , `PUT`, `DELETE`
-   **Path**: /api/queries/patients

    Input:

    ```json
    {
    "account_uid": str,
    "f_name": str,
    "l_name": str,
    "email": str,
    "address": str,
    "phone": str,
    "dob": date,
    "p_initial": str
    }
    ```

    Output:

    ```json
    {
    "id": int,
    "uid": str,
    "account_uid": str,
    "f_name": str,
    "l_name": str,
    "email": str,
    "address": str,
    "phone": str,
    "dob": date,
    "p_initial": str
    }
    ```

*   The Patients API allows access to individual patient records or all patients at once. Users can create, update, and delete patients.

# Provider

-   **Method**: `GET`, `GET`, `POST` , `PUT`, `DELETE`
-   **Path**: /api/queries/provider

    Input:

    ```json
    {
    "name": str,
    "email": str,
    "specialty": str,
    "phone": str
    }
    ```

    Output:

    ```json
    {
    "id": int,
    "uid": str,
    "name": str,
    "email": str,
    "specialty": str,
    "phone": str
    }
    ```

*   The Provider API allows users to retrieve either a single provider or all providers at once. Also they are able to create, update, and delete provider records.
